package lt.mattdevleops.objects;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.rey.material.widget.SnackBar;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        User user = new User();
        user.setName("Alfonsas");
        Log.d("CONSOLE","BANDZIAU PAKEISTI VARDA I ALFONSAS, GAVAU KAD DABARTINIS VARDAS YRA " + user.getName());
        user.setName("Matas");
        Log.d("CONSOLE","BANDZIAU PAKEISTI VARDA I ALFONSAS, GAVAU KAD DABARTINIS VARDAS YRA " + user.getName());
        ConnectivityManager cm = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();

        if(activeNetwork != null && activeNetwork.isConnectedOrConnecting()){

            SnackBar.make(this)
                    .singleLine(true)
                    .text("Hello World")
                    .duration(10000)
                    .actionText("Close this")
                    .actionClickListener(new SnackBar.OnActionClickListener() {
                        @Override
                        public void onActionClick(SnackBar sb, int actionId) {

                            SnackBar.make(MainActivity.this)
                                    .singleLine(true)
                                    .text("Why did you close this")
                                    .duration(3000)
                                    .actionText("Bye")
                                    .show(MainActivity.this);

                        }
                    }).show(this);

        }


    }
}
